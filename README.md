# FastQC Plot

A tool to plot FastQC log files using Plotly.

Incorporates code from Adam Labadorf's fastqcparser project. https://pypi.org/project/fastqcparser/