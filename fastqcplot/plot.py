
from .parser import FastQCParser
import plotly.graph_objects as go
from plotly.offline import plot
import plotly.express as px
import numpy as np

def fig_to_html(fig, include_plotlyjs=False):
    return plot(fig, output_type='div', include_plotlyjs=include_plotlyjs, show_link=False, link_text="" )


class FastQCPlot(object):
    def __init__(self, fastqc_file, plot_bgcolor="#eeeeee"):
        self.parser = FastQCParser(fastqc_file)
        self.plot_bgcolor = plot_bgcolor

    def fig_per_base_sequence_quality(self, width=1200, height=600, **kwargs):
        df = self.parser.dataframe('Per base sequence quality')

        fig = px.line(df, x="Base", y="Mean", title=f'Quality scores across all bases ({self.parser.encoding} encoding)', labels={
                            "Base": "Position in read (bp)",
                            "Mean": "Quality Score",
                        },
        )

        fig.add_trace(go.Box(
            median=df['Median'], 
            q1=df['Lower Quartile'], 
            q3=df['Upper Quartile'], 
            lowerfence=df['10th Percentile'], 
            upperfence=df['90th Percentile'],
            marker_color = 'black',
            fillcolor = 'yellow',
            opacity=0.5,
            whiskerwidth=1.0,
            x=df['Base'],
        ))
        fig.update_layout(showlegend=False)
        max_y = max(df['90th Percentile'].max(), 35.0)
        fig.update_yaxes(range=[0, max_y])

        fig.update_layout(
            shapes=[
                # 1st highlight during Feb 4 - Feb 6
                dict(
                    type="rect",
                    # x-reference is assigned to the plot paper [0,1]
                    xref="paper",
                    # y-reference is assigned to the y
                    yref="y",
                    x0=0,
                    y0=0,
                    x1=1.0,
                    y1=20,
                    fillcolor="rgb(222, 177, 177)",
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                ),
                dict(
                    type="rect",
                    # x-reference is assigned to the plot paper [0,1]
                    xref="paper",
                    # y-reference is assigned to the y
                    yref="y",
                    x0=0,
                    y0=20,
                    x1=1.0,
                    y1=28,
                    fillcolor="rgb(227, 215, 168)",
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                ),
                dict(
                    type="rect",
                    # x-reference is assigned to the plot paper [0,1]
                    xref="paper",
                    # y-reference is assigned to the y
                    yref="y",
                    x0=0,
                    y0=28,
                    x1=1.0,
                    y1=40,
                    fillcolor="rgb(186, 228, 180)",
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                ),
            ]
        )
        fig.layout.plot_bgcolor = "white"
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig
    
    def fig_per_sequence_quality_scores(self, width=1200, height=600, **kwargs):
        df = self.parser.dataframe('Per sequence quality scores')

        fig = px.line(df, x="Quality", y="Count", title='Quality score distribution over all sequences', color_discrete_sequence=["red"],
            labels={
                "Quality": "Mean Sequence Quality (Phred Score)",
            },
        )
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig_per_base_sequence_content(self, width=1200, height=600, **kwargs):
        module_name = "Per base sequence content"
        df = self.parser.dataframe(module_name)

        fig = px.line(df, x="Base", y=["T","C","A","G"], title='Sequence content across all bases', 
            color_discrete_sequence=["red", "blue", "Green", "Black"], 
            labels={
                "Base": "Position in read (bp)",
                "value": "Percentage",
                "variable": "Nucleobase",
            },
        )
        fig.update_yaxes(range=[0, 100])
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig_per_sequence_gc_content(self, width=1200, height=600, **kwargs):
        module_name = "Per sequence GC content"
        df = self.parser.dataframe(module_name)

        from scipy.stats import norm
        
        # fastqc defines the middle of the GC count distribution as the average of GC% values 
        # in the contiguous region around the mode 
        # where the distrubution is over 90% of the max. 
        # (NB the comments in the code say 95% but the code corresponds to 90%)
        # if the distribution is skewed so that then it uses just the mode

        # If we were to use the mean, we could have:
        # mean = np.average(x, weights=df.Count)

        x = df["GC Content"]
        y = df["Count"]
        max_count = y.max()
        upper_limit = max_count * 0.90
        mode_index = np.argmax(y)
        mode = x[mode_index]

        middle_region = [mode]
        fell_off_top = True
        for index in range(mode+1, len(x)):
            if y[index] > upper_limit:
                middle_region.append(x[index])
            else:
                fell_off_top = False
                break
        
        fell_off_bottom = True
        for index in range(mode-1, 0, -1):
            if y[index] > upper_limit:
                middle_region.append(x[index])
            else:
                fell_off_bottom = False
                break

        if fell_off_bottom or fell_off_top:
            middle = mode
        else:
            middle = np.mean( middle_region )
        
        variance = np.average((x-middle)**2, weights=df.Count)
        std = np.sqrt(variance)
        total = np.sum(df.Count)

        df['theoretical'] = norm.pdf(x, loc=middle, scale=std)*total

        fig = px.line(df, x="GC Content", y=["Count","theoretical"], title='GC distribution over all sequences', 
            labels={
                "GC Content": "Mean GC content (%)",
                "Count": "GC count per read",
                "theoretical": "Theoretical Distribution",
                "value": "GC count",
                'variable':"",
            },
        )
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            legend=dict(
                yanchor="top",
                y=0.99,
                xanchor="right",
                x=0.99,
            )
        )
        return fig

    def fig_per_base_N_content(self, width=1200, height=600, **kwargs):
        module_name = "Per base N content"
        df = self.parser.dataframe(module_name)

        fig = px.line(df, x="Base", y="N-Count", 
            title='N content across all bases', 
            color_discrete_sequence=["red"], 
            labels={
                "Base": "Position in read (bp)",
                "N-Count": "Percentage N",
            },
        )
        fig.update_yaxes(range=[0, 100])
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig_sequence_length_distribution(self, width=1200, height=600, **kwargs):
        module_name = "Sequence Length Distribution"
        df = self.parser.dataframe(module_name)

        single_value = (len(df.index) == 1)

        plot = px.bar if single_value else px.line
        fig = plot(df, x="Length", y="Count", 
            title='Distribution of sequence lengths over all sequences', 
            color_discrete_sequence=["red"], 
            labels={
                "Length": "Squence Length (bp)",
            },
        )
        if single_value:
            fig.update_xaxes(type='category')
            fig.update_traces(width=[0.1],opacity=0.6)

        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig_sequence_duplication_levels(self, width=1200, height=600, **kwargs):
        module_name = "Sequence Duplication Levels"
        df = self.parser.dataframe(module_name)

        fig = px.line(df, x="Duplication Level", y=["Percentage of deduplicated", "Percentage of total"], 
            title='Percentage of seqs remaining if deduplicated 92.38%', 
            color_discrete_sequence=["red", "blue"], 
            labels={
                "value":"Percentage",
                "Percentage of deduplicated": "% Deduplicated sequences",
                "Percentage of total": "% Total sequences",
                "variable": "",
            },
        )
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            xaxis={'type': 'category'},
            legend=dict(
                yanchor="top",
                y=0.99,
                xanchor="right",
                x=0.99,
            )
        )
        return fig

    def fig_adapter_content(self, width=1200, height=600, **kwargs):
        module_name = "Adapter Content"
        df = self.parser.dataframe(module_name)

        fig = px.line(df, x="Position", y=df.columns[1:], 
            title='% Adapter', 
            color_discrete_sequence=["red", "blue", "Green", "Black", "Magenta"], 
            labels={
                "Position":"Position in read (bp)",
                "variable": "",
                "value": "Percentage Adapter",
            },
        )
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig_per_tile_sequence_quality(self, width=1200, height=600, **kwargs):
        module_name = "Per tile sequence quality"
        df = self.parser.dataframe(module_name)

        tiles = df.Tile.unique()

        bases = df.Base.unique()
        mean_values = np.zeros( (len(tiles),len(bases)))
        for x_index, tile in enumerate(tiles):
            mean_values[x_index] = df[df.Tile == tile]["Mean"]

        fig = px.imshow(mean_values, x=bases, y=tiles, title='Quality per tile',origin="lower",
            labels=dict(x="Position in read (bp)", y="Tile", color="Quality"),
            aspect="auto",
        )
        fig.layout.plot_bgcolor = self.plot_bgcolor
        fig.update_yaxes(type='category')
        fig.update_layout(
            autosize=False,
            width=width,
            height=height,
            **kwargs,
        )        
        return fig

    def fig(self, module_name, **kwargs):
        fig_func_dict = {
            'Per base sequence quality': self.fig_per_base_sequence_quality,
            'Per sequence quality scores': self.fig_per_sequence_quality_scores,
            'Per base sequence content': self.fig_per_base_sequence_content,
            'Per sequence GC content': self.fig_per_sequence_gc_content,
            'Per base N content': self.fig_per_base_N_content,
            'Sequence Length Distribution': self.fig_sequence_length_distribution,
            'Sequence Duplication Levels': self.fig_sequence_duplication_levels,
            'Adapter Content': self.fig_adapter_content,
            'Per tile sequence quality': self.fig_per_tile_sequence_quality,
        }
        if module_name in fig_func_dict:
            return fig_func_dict[module_name](**kwargs)
        return None

    def html(self, module_name, include_plotlyjs=False, **kwargs):
        fig = self.fig(module_name, **kwargs)
        if fig:
            return fig_to_html(fig, include_plotlyjs=include_plotlyjs)

        df = self.parser.dataframe(module_name)
        return df.to_html(classes="table table-hover", border=0)

