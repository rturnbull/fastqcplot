from setuptools import setup

requirements = [
    'pandas>=1.0.5',
    'plotly>=4.9.0',
    'scipy',
]

setup(
    install_requires=requirements,
)