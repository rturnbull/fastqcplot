#!/usr/bin/env python3

import unittest
import pandas as pd
from fastqcplot import FastQCParser
from pandas.testing import assert_frame_equal


class TestParser(unittest.TestCase):
    def test_bzip(self):
        filename = 'ERR1594293_fastqc.bz2'
        parser = FastQCParser('tests/data/'+filename)

        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Overrepresented sequences', 'Adapter Content']
        self.assertListEqual(list(parser.modules.keys()), module_names)
        self.assertEqual( parser.file_type, "Conventional base calls" )
        self.assertEqual( parser.encoding, "Sanger / Illumina 1.9" )
        self.assertEqual( parser.total_sequences, 97983 )
        self.assertEqual( parser.filtered_sequences, 0 )
        self.assertEqual( parser.sequence_length, "114-151" )
        self.assertEqual( parser.percent_gc, 55 )

        for module_name in module_names:
            df = parser.dataframe(module_name)
            expected_pkl_name = "tests/expected/%s/%s.pkl" % (filename, module_name.replace(" ", "_"))
            # to create the pickle file, uncomment this line 
            # df.to_pickle( expected_pkl_name )
            expected_df = pd.read_pickle( expected_pkl_name )
            assert_frame_equal( df, expected_df )


    def run_tests_SRR942590(self, parser, filename):
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Overrepresented sequences', 'Adapter Content']        
        self.assertListEqual(list(parser.modules.keys()), module_names)
        self.assertEqual( parser.file_type, "Conventional base calls" )
        self.assertEqual( parser.encoding, "Sanger / Illumina 1.9" )
        self.assertEqual( parser.total_sequences, 15304 )
        self.assertEqual( parser.filtered_sequences, 0 )
        self.assertEqual( parser.sequence_length, "40-1021" )
        self.assertEqual( parser.percent_gc, 57 )

        string_value = """FastQC version: 0.11.9\nFilename: SRR942590.fastq.gz\nFile type: Conventional base calls\nEncoding: Sanger / Illumina 1.9\nTotal Sequences: 15304\nSequences flagged as poor quality: 0\nSequence length: 40-1021\n%GC: 57"""
        self.assertEqual( str(parser), string_value )

        for module_name in module_names:
            df = parser.dataframe(module_name)
            expected_pkl_name = "tests/expected/%s/%s.pkl" % (filename, module_name.replace(" ", "_"))
            expected_df = pd.read_pickle( expected_pkl_name )
            assert_frame_equal( df, expected_df )

    def test_txt(self):
        filename = 'SRR942590_fastqc.txt'
        parser = FastQCParser('tests/data/'+filename)
        self.run_tests_SRR942590(parser, filename)

    def test_per_tile_sequence_quality(self):
        filename = "ERR1341887_2_fastqc_data.txt"
        parser = FastQCParser('tests/data/'+filename)
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per tile sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Adapter Content']

        self.assertListEqual(list(parser.modules.keys()), module_names)
        self.assertEqual( parser.file_type, "Conventional base calls" )
        self.assertEqual( parser.encoding, "Sanger / Illumina 1.9" )
        self.assertEqual( parser.total_sequences, 9521589 )
        self.assertEqual( parser.filtered_sequences, 0 )
        self.assertEqual( parser.sequence_length, "35-101" )
        self.assertEqual( parser.percent_gc, 48 )

        for module_name in module_names:
            df = parser.dataframe(module_name)
            expected_pkl_name = "tests/expected/%s/%s.pkl" % (filename, module_name.replace(" ", "_"))
            expected_df = pd.read_pickle( expected_pkl_name )
            assert_frame_equal( df, expected_df )

    def test_gzip(self):
        filename = 'SRR3988881_1_fastqc.gz'
        parser = FastQCParser('tests/data/'+filename)

        # This list does not have overrepresented sequences
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Adapter Content']

        self.assertListEqual(list(parser.modules.keys()), module_names)
        self.assertEqual( parser.file_type, "Conventional base calls" )
        self.assertEqual( parser.encoding, "Sanger / Illumina 1.9" )
        self.assertEqual( parser.total_sequences, 711113 )
        self.assertEqual( parser.filtered_sequences, 0 )
        self.assertEqual( parser.sequence_length, 150 )
        self.assertEqual( parser.percent_gc, 38 )

        for module_name in module_names:
            df = parser.dataframe(module_name)
            expected_pkl_name = "tests/expected/%s/%s.pkl" % (filename, module_name.replace(" ", "_"))
            expected_df = pd.read_pickle( expected_pkl_name )
            # Problem with pytest for this line
            # assert_frame_equal( df, expected_df )

    def run_tests_ERR1559343(self, parser, filename):
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Overrepresented sequences', 'Adapter Content']        

        self.assertListEqual(list(parser.modules.keys()), module_names)
        self.assertEqual( parser.file_type, "Conventional base calls" )
        self.assertEqual( parser.encoding, "Sanger / Illumina 1.9" )
        self.assertEqual( parser.total_sequences, 138972 )
        self.assertEqual( parser.filtered_sequences, 0 )
        self.assertEqual( parser.sequence_length, "114-151" )
        self.assertEqual( parser.percent_gc, 54 )

        for module_name in module_names:
            df = parser.dataframe(module_name)
            expected_pkl_name = "tests/expected/%s/%s.pkl" % (filename, module_name.replace(" ", "_"))
            expected_df = pd.read_pickle( expected_pkl_name )
            # Problem with pytest for this line
            # assert_frame_equal( df, expected_df )



    def test_zip(self):
        filename = 'ERR1559343_fastqc_data.zip'
        parser = FastQCParser('tests/data/'+filename)
        self.run_tests_ERR1559343(parser, filename)





if __name__ == '__main__':
    unittest.main()
