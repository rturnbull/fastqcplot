#!/usr/bin/env python3

import unittest
import pandas as pd
from fastqcplot import FastQCPlot
from pandas.testing import assert_frame_equal
from plotly.io import to_json
import json


class TestPlot(unittest.TestCase):
    def plotting_tests(self, filename, module_names, generate_expected=False):
        plot = FastQCPlot('tests/data/'+filename)
        self.assertListEqual(list(plot.parser.modules.keys()), module_names)

        for module_name in module_names:
            fig = plot.fig( module_name )
            if fig:
                json_data = to_json(fig)
                data = json.dumps(json_data, sort_keys=True)
                html = plot.html( module_name )
                self.assertRegex( html, "<div")
            else:
                data = plot.html( module_name )
            expected_name = "tests/expected/%s/%s.dat" % (filename, module_name.replace(" ", "_"))
            # Generate if necessary
            if generate_expected:
                with open(expected_name, 'w' ) as f:
                    f.write(data)

            with open(expected_name, 'r' ) as f:
                expected_data = f.read()
            
                self.assertEqual( data, expected_data )

    def test_ERR1594293(self):
        filename = 'ERR1594293_fastqc.bz2'
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Overrepresented sequences', 'Adapter Content']
        self.plotting_tests( filename, module_names )

    def test_ERR1341887_2(self):
        filename = 'ERR1341887_2_fastqc_data.txt'
        module_names = ['Basic Statistics', 'Per base sequence quality', 'Per tile sequence quality', 'Per sequence quality scores', 'Per base sequence content', 'Per sequence GC content', 'Per base N content', 'Sequence Length Distribution', 'Sequence Duplication Levels', 'Adapter Content']
        self.plotting_tests( filename, module_names, generate_expected=False )


if __name__ == '__main__':
    unittest.main()
